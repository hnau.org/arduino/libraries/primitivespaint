#include "PrimitivesPaint.h"



PrimitivesPaint::PrimitivesPaint() {
	color = RGBAs::black;
}

void PrimitivesPaint::setColor(RGBA color) {
	this->color = color;
}

void PrimitivesPaint::fill(Canvas& canvas) {
	for (size_t ix = 0; ix < canvas.getWidth(); ix++) {
		for (size_t iy = 0; iy < canvas.getHeight(); iy++) {
			canvas.setPixelColor(ix, iy, color);
		}
	}
}
