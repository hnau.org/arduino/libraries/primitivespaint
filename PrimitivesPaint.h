#ifndef PRIMITIVESPAINT_H
#define PRIMITIVESPAINT_H

#include <RGBA.h>
#include <Font.h>
#include <Canvas.h>
#include <CharsProvider.h>

class PrimitivesPaint {
	
private:

	RGBA color;
	

public:

	PrimitivesPaint();
	
	void setColor(RGBA color);
	
	void fill(Canvas& canvas);

};


#endif //PRIMITIVESPAINT_H
